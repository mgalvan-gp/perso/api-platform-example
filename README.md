# API Platform example

API de produits et d'offres associées

Le but :
* Tester API Platform

Technologies utilisées :
* PHP
    * Symfony - Doctrine

Que fait l'API ?
* Ajoute et modifie des offres et des produits
* Sélectionne un(e) ou tout(e)s les offres et les produits
* Suprime des offres et des produits

Comment ça marche ?
* Cloner le projet avec `git clone`
* lancer `composer update` puis `symfony serve`
* Aller sur `localhost:8000/api/`
